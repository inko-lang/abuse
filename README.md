# README

This project, and mostly its issue tracker, can be used to report instances
of unacceptable behaviour by members of the Inko community.

For more information about what is considered "unacceptable behaviour", refer to
the [Inko Code of Conduct](https://inko-lang.org/code-of-conduct).

## How to report an issue

First of all, we are sorry you have had a bad experience. We will do our best to
resolve the issue in a timely manner. To report an instance of abuse, take the
following steps:

1. [Create a **confidential** issue](https://gitlab.com/inko-lang/abuse/issues/new)
   (make sure the "This issue is confidential and should only be visible to team members with at least Reporter access." checkbox is checked).
1. Clearly describe what problem you ran into, the people involved, the
   location, etc.
1. Include some form of proof, such as screenshots or links to public
   conversations.
